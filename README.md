## Blitzers purpose
is streamlining the management of systems and distribution of software to many servers like in cloud environments, iot, big data, or in general any datacenter.

Main site: [https://intrasts.net/blz/index.html](https://intrasts.net/blz/index.html)

Blitzer features:
- parallel execution
- realtime progress visualisation
- cascading distribution
- rolling updates

Besides taking care of the complicated __parallel execution__ and distribution,
__Blitzer__ provides a simple, immersive web interface, 
wich lets users write, test, persist and run scripts in one place.

## Model
Blitzer is build around the fundamental resource model contsisting of:
1. server nodes along with their access credentials
2. components of payload software
    
## Scope
Users define this model and can then run scripts against all or selected parts in the three scopes:
- per node,
- per component or
- per instance (node-component pair)
    
## Execution modes
- single:  
        to run once on the Blitzer node
- parallel:  
        run all at the same time
- sequential:  
        one node/instance/comp after the other
- rolling or 2/3 available:  
        only on one thrid of all nodes/instances at a time in order to keep 
        the other two thirds up

## Distribution modes
Blitzer introduces a cascading distribution mode similar to bittorrent in order to maximize the network utilisation. 
This speeds up file transfer jobs substantially in contrary to the bottleneck when simply serving a file from one node to all others.  
With Blitzers distribution mode each node wich received the file acts as a server to the next nodes until all nodes have it.

There are two modes, linear and random. In linear mode the nodes wich receive first
are evenly spread and the next ones are close to these. This resembles the strategy to 
transfer first to a node in each rack and then between the nodes inside each rack.

Also Blitzer offers the **random distribution** when topology suits this, i.e. connects 
all nodes similarly near/fast.



        
