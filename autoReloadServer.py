
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import SocketServer
#import simplejson
import random

class S(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self._set_headers()
        f = open("static/index.html", "r")
        self.wfile.write(f.read())

    def do_HEAD(self):
        self._set_headers()

    def do_POST(self):
        #self._set_headers()
        print "in post method"
        self.data_string = self.rfile.read(int(self.headers['Content-Length']))


        #data = simplejson.loads(self.data_string)
        #print self.data_string


        print self.headers['Content-Length']
        print self.data_string

        DATA = """{ "isHVV":true }"""
        self.send_response(200)
        self.send_header("content-type", "application/json")
        self.send_header("content-length", len(DATA))
        self.wfile.write(DATA)

        return


def run(server_class=HTTPServer, handler_class=S, port=8000):
    server_address = ('localhost', port)
    httpd = server_class(server_address, handler_class)
    print 'Starting http://%s:%d/' % (server_address[0], port)
    httpd.serve_forever()

if __name__ == "__main__":
    from sys import argv

if len(argv) == 2:
    run(port=int(argv[1]))
else:
    run()

