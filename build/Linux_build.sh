#!/bin/bash

#export PP="/home/mega/eclipse/plugins/org.python.pydev_5.0.0.201605051159/pysrc/pydev_sitecustomize:/home/mega/Documents/bluespace:/usr/lib/python2.7:/usr/lib/python2.7/plat-x86_64-linux-gnu:/usr/lib/python2.7/lib-tk:/usr/lib/python2.7/lib-dynload:/usr/local/lib/python2.7/dist-packages:/usr/lib/python2.7/dist-packages:/usr/lib/python2.7/dist-packages/PILcompat:/usr/lib/python2.7/dist-packages/gtk-2.0:/usr/lib/python2.7/dist-packages/ubuntu-sso-client"
# -p $PP

PN=blitzer
OS=linux
VS=`head -1 ../CHANGELOG |cut -d " " -f 1`
DN=$PN"_"$OS"_"$VS

~/Downloads/PyInstaller-3.2/pyinstaller.py -F -n $PN \
	-c --distpath ./$DN \
	--osx-bundle-identifier $PN.rocks \
	--noupx ../$PN.py

cp -r ../static $DN/
mkdir $DN/scr
mkdir $DN/env
mkdir $DN/upload

zip -r $DN.zip $DN