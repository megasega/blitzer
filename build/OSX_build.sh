#!/bin/bash

PN=blitzer
OS=macosx
VS=`head -1 ../CHANGELOG |cut -d " " -f 1`
DN=$PN"_"$OS"_"$VS

~/Downloads/PyInstaller-3.2/pyinstaller.py -F -n $PN \
	-c --distpath ./$DN \
	--osx-bundle-identifier $PN.rocks \
	--noupx ../$PN.py

cp -r ../static $DN/
mkdir $DN/scr
mkdir $DN/env
mkdir $DN/upload

zip -r $DN.zip $DN