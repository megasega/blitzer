

function doapp() {
	// get folders
	// blocks
	document.getElementById('begin').style.display = 'none'
	console.log('doapp')
	document.getElementById('gst').style.display = 'block'
		x = window.innerWidth / 6 *1.15 
		y = x 
		$(".gridster ul").gridster({
			widget_margins : [ 1, 1 ],
			widget_base_dimensions : [ x, y ]
		});
	folders()
}

function doMail(Mid){
	x1 = xreq('get', '/cmail/mail/' + Mid  )
	
	x1.then(function(response) {
		  if( response.ok ){
			  //var mail = unescape(response.mail)
			  var mail = response.mail
			  var mplain = (mail.plain)
			  var mhtml = (mail.html)
			  document.getElementById('mail').innerHTML = eac('div','',mhtml || mplain)
		  } else{
			  console.log('get mail fail')
		  }
	});
}

function doBlock(fid, blid){
	x1 = xreq('get', '/cmail/block/' + fid + '/' + blid )
	
	x1.then(function(response) {
		  if( response.ok ){
			  var block = response.block
			  tbl = '<table width="100%"><tr>'
			  var head = ['from','to','subject','date','att','size']
			  for(var i in head){
				  tbl += eac('th','',head[i])
			  }
			  tbl += '</tr>'
			  var bln = block.split('^_')
			  for( var i in bln ){
				  if( bln.length < 3 ) break;
				  var rw = ""
				  var row = bln[i].split('_^')
				  rw += eac('td','',row[0])	
				  rw += eac('td','',row[1])	
				  rw += eac('td','',row[2])	
				  rw += eac('td','',row[3])	
				  rw += eac('td','',row[5])	
				  rw += eac('td','',row[6])	
				  tbl += eac('tr','onClick="doMail(\''+row[7]+'\');"',rw)
			  }
			  tbl += '</table>'
			  document.getElementById('blx').innerHTML = tbl
		  } else{
			  console.log('get block fail')
		  }
	});
}

function folders(){
	x1 = xreq('get', '/cmail/folders' )
	
	x1.then(function(response) {
		  if( response.ok ){
			  folders = response.folders
			  var sf = folders.split(';')
			  var ret = ""
			  for( var fn in sf ){
				  var fid = sf[fn].split(',')[0]
				  var fname = sf[fn].split(',')[1]
				  var blid = sf[fn].split(',')[2]
				  ret += eac('div','href="#" onClick="doBlock('+fid+','+blid+')"',fname)
				  //ret += "<br>"
			  }
			  document.getElementById('folders').innerHTML = ret
  			  doBlock("0", "0")

		  } else{
			  console.log('get folders fail')
		  }
	});
}


// users folders blocks mails attachments
var token = 'xoxoxoxoxoxoxoxoxoxoxoxoxox'


function xreq(verb, url, dat, contenttype){
	return new Promise(function(resolve, reject) {
		var req = new XMLHttpRequest();
		req.open(verb, url);
		req.setRequestHeader('Token',token );
		if(dat){
			contenttype = "application/x-www-form-urlencoded; charset=UTF-8"
		}
		req.setRequestHeader("Content-type", contenttype || "application/json");
		req.onload = function() { // This is called even on 404 etc., so check the status
			if (req.status == 200) { // Resolve the promise with the response text
				resolve(req.response);
				document.getElementById('xreq').style.display='none'
			} else {
				reject(Error(req.statusText));
				document.getElementById('xreq').style.display='none'
			}
		};
		req.onerror = function() { // Handle network errors
			reject(Error("Network Error"));
			document.getElementById('xreq').style.display='none'
		};
		document.getElementById('xreq').style.display='inline'
		if(dat){
			//console.log('hasdat')
			req.send(dat);
		} else{
			//console.log('no dat')
			req.send(); 
		}
	}).then(function(response) {
  	      console.log( 'hiho1',response );
		var ret = JSON.parse(response);
		token = ret.token;
		return ret;
		});
}
var currentUser = 'bla@bla.net';
navigator.id.watch({
	loggedInUser : currentUser,
	onlogin : function(assertion) {
		//console.log("sec assert:",assertion)
		x1 = xreq('post', '/auth/login', 'assertion=' + assertion  )
			
		x1.then(function(response) {
			  //console.log( 'hiho2',response );
			  if( response.ok ){
				  token = response.token
				  document.getElementById('login').innerHTML = JSON.stringify(response)
				  doapp()
			  } else{
				  alert('login fail')
			  }
		});
	},
	onlogout : function() {
		x1 = xreq('post', '/auth/logout' )
		token = ''
	}
});


/*  
 
$.ajax({
	type : 'POST',
	url : '/auth/login', // This is a URL on your website.
	data : {
		assertion : assertion
	},
	success : function(res, status, xhr) {
		// alert(res + " " + status + " " + xhr)
		document.getElementById('login').innerHTML = res
		doapp();
		// window.location.reload();
	},
	error : function(xhr, status, err) {
		navigator.id.logout();
		alert("Login failure: " + err);
	}
});

		x1.then(function(response) {
			console.log('ss',response)
			if( response.ok ){
				alert("log out ok");
			}
		});
		$.ajax({
			type : 'POST',
			url : '/auth/logout', // This is a URL on your website.
			success : function(res, status, xhr) {
				// window.location.reload();
			},
			error : function(xhr, status, err) {
				alert("Logout failure: " + err);
			}
		});


var promiseCount = 0;
function testPromise() {
  var thisPromiseCount = ++promiseCount;

  var log = document.getElementById('log');
  log.insertAdjacentHTML('beforeend', thisPromiseCount + 
      ') Started (<small>Sync code started</small>)<br/>');

  // Wir erstellen einen neuen Promise: wir versprechen den String 'result' (Wartezeit max. 3s)
  var p1 = new Promise(
    // Resolver-Funktion kann den Promise sowohl auflösen als auch verwerfen
    // reject the promise
    function(resolve, reject) {       
      log.insertAdjacentHTML('beforeend', thisPromiseCount + 
          ') Promise started (<small>Async code started</small>)<br/>');
      // nur ein Beispiel, wie Asynchronität erstellt werden kann
      window.setTimeout(
        function() {
          // We fulfill the promise !
          resolve(thisPromiseCount)
        }, Math.random() * 2000 + 1000);
    });

  // wir bestimmen, was zu tun ist, wenn der Promise fulfilled
  p1.then(
    // Loggen der Nachricht und des Wertes
    function(val) {
      log.insertAdjacentHTML('beforeend', val +
          ') Promise fulfilled (<small>Async code terminated</small>)<br/>');
    });

  log.insertAdjacentHTML('beforeend', thisPromiseCount + 
      ') Promise made (<small>Sync code terminated</small>)<br/>');
}

 */