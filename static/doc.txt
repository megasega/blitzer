
JS: JSON.parse(response); JSON.stringify(response)
PY: json.dumps() json.loads()
# cat ~/.ssh/id_rsa.pub | ssh dionys@192.168.0.94 'umask 0077; mkdir -p .ssh; cat >> .ssh/authorized_keys && echo "Key copied"'

ssh-keyscan  <newhost> >> ~/.ssh/known_hosts

for h in $SERVER_LIST; do
    ip=$(dig +short $h)
    ssh-keygen -R $h
    ssh-keygen -R $ip
    ssh-keyscan -H $ip >> ~/.ssh/known_hosts
    ssh-keyscan -H $h >> ~/.ssh/known_hosts
done

ssh pi@192.168.0.87
ssh mega@192.168.0.94

ssh -oStrictHostKeyChecking=no sai uptime
python -m SimpleHTTPServer 8000
twistd -n web -p 8000 --path .
busybox httpd -f -p 8000
webfsd -F -p 8000

http://tympanus.net/Tutorials/TypographyEffects/
http://tympanus.net/codrops/

"""
import socket
import fcntl
import struct

def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])

def getHwAddr(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    info = fcntl.ioctl(s.fileno(), 0x8927,  struct.pack('256s', ifname[:15]))
    return ''.join(['%02x:' % ord(char) for char in info[18:24]])[:-1]    

print socket.gethostbyname(socket.gethostname())
"""

""" 
to make the /tmp reside in ramdisk in linux edit the /etc/fstab and add the line
tmpfs           /tmp    tmpfs   nosuid  0       0
Then reboot or run "mount -a" as root
"""

#d = datetime.today()
#job_id = d.strftime("%Y%m%d_%H%M%S_%f")
#envdat = getdat( env, 'env' ).split('\n')
#runpopen(path, cmd)
# 'status']['all' :: host, app, code, msg
