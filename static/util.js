
function isMobile(){
	//if( /(MacIntel)/i.test(navigator.platform) )
	return /(android|iphone|ipad|blackberry|ipod|kindle)/i.test(navigator.userAgent.toLowerCase() );
}

var LNG = 'EN'; // need to set LNG var somehow
var langset = false;

function setlang(){
	var xlanguage = window.navigator.userLanguage || window.navigator.language;
	//assert(xlanguage); //works IE/SAFARI/CHROME/FF
	//assert('hi all', id, navigator.language);
	//assert('hi all', id, navigator.userLanguage);
	//if(language){
		lx = xlanguage.substr(0,2).toUpperCase();
		//assert(lx);
		if( 'DE EN ES PT FR NL IT'.indexOf(lx) > -1 ){
			//assert('langselect ', lx);
			LNG = lx;
		}
	//}
	langset = true;
	//LNG = 'NL'; // just to test
}

function st(id){
	if( ! langset ) setlang();
	ret = i18n[ LNG + '_' + id ];
	return ret.length == 0 ? i18n[ 'EN_' + id ] : ret;
}
function o2l(obj){ 
	// lists the attribute names or keys of an object
	var arr = [];
	for(var x in obj){
		arr.push(x);
	}
	return arr.sort();
}
function inlist(s, list){
	for(var i in list){
		if(s == list[i]) return true;
	}
	return false;
}
function removeFromList(list, s){
	ret = [];
    for(var i in list){
    	var a = list[i]
        if(s == a ) {}
        else { ret.push( a ) };
    }
    return ret;
}
function eac(ent, attr, cont ){
    return '\n<' + ent + ' ' + (attr || '') + '>' + cont + '</' + ent + '>';
};
function mbu(name, att, func ){
    return eac('div', att + ' id="butt_'+name+'" onClick="'+func+'();" ', name );
}
function mbup(name, att, func, param ){
    return eac('div', att + ' id="butt_'+name+'" onClick="'+func+'(\''+param+'\');" ', name );
}
function eic(ent, id, cont){
    var e = document.createElement(ent);
	e.setAttribute('id', id);
	e.innerHTML= cont ;
	return e;
};
function dge(id){
	return document.getElementById(id)
};
function table(rows, attr, firstishead ){
	var fh = firstishead || false;
	var div = 'td'
	for(var i in rows){
		if( i == 0 && firstishead){ div = 'th'} else{ div = 'td' }
		var rr = ""
		for(var ri in rows[i]){
			rr += eac(div,'',rows[i][ri])
		}
		ret += eac('tr','', rr)
	}
	return eac( 'table', attr || '', ret) ;
};

function assertToggle( id ){
	var ass = document.getElementById(id);
	var full = ass.getAttribute('full');
	if( full == 'false' ){
		ass.style.width='100%';
		ass.style.height='100%';
		ass.style.opacity='.9';
		ass.style.wordBreak='break-all';
		ass.style.zIndex='1111';
		ass.setAttribute('full', 'true');
		if( id == "assertred" )
			ass.style.top = '0px';
	}
	else{
		if( id == "assertred" )
			ass.style.top = '50%';
		ass.style.opacity='.6';
		ass.style.zIndex='1';
		ass.style.width='33%';
		ass.style.height='50%';
		ass.setAttribute('full', 'false');
	}
	setTimeout("window.getSelection().removeAllRanges()", 20);
}
function putassert( li, id ){
	var ass = document.getElementById(id);
	if (ass) {
		//var fc = ass.firstChild;
		ass.insertBefore(li, ass.firstChild);
		//ass.appendChild(li);
	} else {
		ass = document.createElement("div");
		ass.setAttribute('id', id);
		ass.setAttribute('full', 'false');
		ass.setAttribute('title', 'double click to toggle full view');
		ass.setAttribute( 'style',
			"list-style-type: none; position: absolute; display: inline; resize: both; right: 0px; top: 50%; "
			+ " z-index: 20; width:33%; height: 50%; overflow: auto;");
		ass.setAttribute( 'ondblclick','assertToggle("' + id + '")');
		if( id == "assertgreen" ){
			ass.style.top = '0px';
		} else {
			ass.style.top = Math.floor(window.innerHeight / 2) + 'px';
		}
		ass.style.background = 'whitesmoke';
		ass.style.opacity = .6;
		document.body.appendChild(ass);
		ass.appendChild(li);
	}
}
function assert(value) {
	var li = document.createElement("li");
	li.style.liststyletype = 'none';
	li.style.color = value ? "green" : "red";
	li.style.marginLeft = '5px';
	var args = Array.prototype.slice.call(arguments);
	var desc = '';
	for (i in args) {
		if (typeof (args[i]) == 'number') {
			ag = Math.floor(args[i] * 10000) / 10000;
			desc += ag + ", ";
		} else {
			desc += args[i] + ", ";
		}
	}
	li.appendChild(document.createTextNode(desc));
	putassert( li, "assertgreen" );
}
function clearasserts(){
	try{document.getElementById('assertgreen').innerHTML='';} catch(e){ ; }
	try{document.getElementById('assertred'  ).innerHTML='';} catch(e){ ; }
}
function assert2(value) {
	var args = Array.prototype.slice.call(arguments);
	var li = '';
	var desc = '';
	li = document.createElement("li");
	li.style.color = args[0] ? "green" : "red";
	li.style.marginLeft = '5px';
	if( args[1]== 999 ){
		desc = document.createElement("pre");
		desc.style.fontFamily = 'courier';
		desc.style.fontSize = '12px';
		desc.style.fontWeight = '600';
		desc.innerHTML = args[2]
		li.appendChild(desc);
	} else{
		for (i in args) {
			if (typeof (args[i]) == 'number') {
				ag = Math.floor(args[i] * 10000) / 10000;
				desc += ag + ", ";
			} else {
				desc += args[i] + ", ";
			}
		}
		li.appendChild(document.createTextNode(desc));
	}
	putassert( li, "assertgreen" );
	if(value){
		putassert( li, "assertgreen" );
	} else {
		putassert( li, "assertred" );
	}
}


function rnd(max){ return Math.ceil( 1000.0 * max * Math.random() / 1000.0);  }

function randStr(num){ 
	var ret = '';
	for(var i = 0 ; i < num; i++){
		ret += String.fromCharCode( 60 + rnd( 26 ) );
	}
	return ret;
}

function ipsum(words, l123 ){
	//document.getElementById('hiho').innerHTML = ipsum(20,3) ;
	switch((l123 | 1) + ""){
		case "1":
			//The standard Lorem Ipsum passage, used since the 1500s
			var l1 = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.".split(' ');
		break;
		case "2":
			//Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC
			var l1 = "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?".split(' ');
			//1914 translation by H. Rackham
			var t2 = "But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?";
		break;
		case "3":
			//Section 1.10.33 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC
			var l1 = "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.".split(' ');
			//1914 translation by H. Rackham
			var t3 = "On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.";
		break;
	}
	var ret = "";
	var len = l1.length;
	var i = u = 0;
	while(u < words ){
		ret += l1[i] + " ";
		i++;
		u++;
		if(i >= len -1) i = 0;
	}
	return ret;
}

function colorful_language(string){

    //alert(string.length);

    if(string.length === 0){
        return "hsl(0, 0, 100%)";
    	//target_element.setAttribute("style","background-color: hsl(0, 0, 100%);");
    }else{
        var sanitized = string.replace(/[^A-Za-z]/, '');
        var letters = sanitized.split('');

        //Determine the hue
        var hue = Math.floor((letters[0].toLowerCase().charCodeAt()-96)/26*360);
        var ord = '';
        for(var i in letters){
            ord = letters[i].charCodeAt();
            if((ord >= 65 && ord <= 90) || (ord >= 97 && ord <= 122)){
                hue += ord-64;
            }
        }

        hue = hue%360;

        //Determine the saturation
        var vowels = ['A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u'];
        var count_cons = 0;

        //Count the consonants
        for(i in letters){
            if(vowels.indexOf(letters[i]) == -1){
                count_cons++;
            }
        }

        //Determine what percentage of the string is consonants and weight to 95% being fully saturated.
        var saturation = count_cons/letters.length/0.95*100;
        if(saturation > 100) saturation = 100;

        //Determine the luminosity
        var ascenders = ['t','d','b','l','f','h','k'];
        var descenders = ['q','y','p','g','j'];
        var luminosity = 50;
        var increment = 1/letters.length*50;

        for(i in letters){
            if(ascenders.indexOf(letters[i]) != -1){
                luminosity += increment;
            }else if(descenders.indexOf(letters[i]) != -1){
                luminosity -= increment*2;
            }
        }
        if(luminosity > 100) luminosity = 100;

        return "hsl("+hue+", "+saturation+"%, "+luminosity+"%)";
        //target_element.setAttribute("style","background-color: hsl("+hue+", "+saturation+"%, "+luminosity+"%);");
    }
}

function isLeap(yr) {
	/*    Every year divisible by 4 is a leap year.
    	  But every year divisible by 100 is NOT a leap year
          Unless the year is also divisible by 400, then it is still a leap year. */

	var x = parseInt(yr);
	if ( x % 4 == 0) {
		if( x % 100 == 0) {
			if( x % 400 == 0) return true;
			else return false;
		}
		else return true;
	}
	else return false;
}

function getMonthMax(mon, yr){
	if( mon == 1){ // Feb
		if( isLeap(yr) ) return 29;
		else return 28;
	}
	ml = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
	return ml[mon];
}

function date2YYYMMDD(date){
	var y = date.getFullYear() ;
    var m = date.getMonth() + 1;
    var mm = m < 10 ? '0' + m : '' + m;
    var d = date.getDate() ;
    var dd = d < 10 ? '0' + d : '' + d;
	//assert( y,mm,d );
	return y + '-' + mm + '-' + dd;
}

function date2HHMM(date){
    var h = date.getHours() ;
    var hh = h < 10 ? '0' + h : '' + h;
    var m = date.getMinutes();
    var mm = m < 10 ? '0' + m : '' + m;
    return hh + ':' + mm;
}

function calendar(fb_func, refday) {
	var days = [ 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat' ];
	var months = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug',
			'Sep', 'Oct', 'Nov', 'Dec' ];

	date = new Date();
	//date.setTime(Date.parse('2013-11-18'));
	date.setTime(Date.parse(refday));
	
	actY = parseInt(date.getFullYear());
	actM = date.getMonth() + 1;
	if( actM > 1 ){ 
		prevM = actM -1; 
		prevY = actY;
	}
	else{ 
		prevM = 12;
		prevY = actY -1;
	}
	pM = prevM < 10 ? '0' + prevM : '' + prevM;
	pYM = prevY + '-' + pM + '-' + '01';
	if( actM < 12 ){
		nextM = actM + 1;
		nextY = actY;
	} 
	else{
		nextM = 1;
		nextY = actY + 1;
	}
	nM = nextM < 10 ? '0' + nextM : '' + nextM;
	nYM = nextY + '-' + nM + '-' + '01';
	//assert(pYM, nYM);
	maxDays = getMonthMax( date.getMonth(), date.getFullYear() );
	first = new Date();
	first.setFullYear(date.getFullYear() );
	first.setMonth(date.getMonth(), 1 );
	//assert( days[first.getDay()] );
	day0 = first.getDay();
	out = '<span style="margin-left: 20px; font-weight: bold; font-size: 1.4em;">';
	out += '<a href="#" onClick="removeElementById(\'cal1\'); removeElementById(\'glass\'); calendar(\'' + fb_func + '\', \'' + pYM + '\');">&nbsp;&lt;&nbsp;</a>  ';
	out += months[date.getMonth()] + ' ';
	out += date.getFullYear();
	out += ' <a href="#" onClick="removeElementById(\'cal1\'); removeElementById(\'glass\'); calendar(\'' + fb_func + '\',\'' + nYM + '\');">&nbsp;&gt;&nbsp;</a></span>  ';
	out += ' <a href="#" onClick="removeElementById(\'cal1\'); removeElementById(\'glass\');">&nbsp;X&nbsp;close</a>';
	out += '<table><tr>\n';
	for(var i = 0 ; i <= 6 ;i++){
		out += '<th>' + days[i] + '</th>';
	}
	out += '</tr><tr>\n';
	for(var i = 1 ; i <= day0 ;i++){
		out += '<td></td>';
	}
	
	for(var i = 1 ; i <= maxDays ;i++){
		first.setMonth(date.getMonth(), i );
		rd = date2YYYMMDD(first);
		out += '<td onClick="' + fb_func + '(\'' + rd + '\');">' + i + '</td>';
		if( first.getDay() == 6 && i < maxDays) out += '</tr>';
	}
	out += '</tr></table>';
	var ass = document.getElementById("cal1");
	if (ass) {}
	else{
		glassDiv();
		var d = document.createElement("div");
		d.setAttribute('id','cal1');
		d.setAttribute('class','cal round_shadow');
		document.body.appendChild(d);
		d.innerHTML = out;
	}
	//assert(days[date.getDay()], date.getDate(), months[date.getMonth()]);
	//assert(date.getDay(), date.getMonth(), date.getFullYear());
}

function ffcheck() {
	// it sucks to code around browser bugs - especially since you never know
	// when ppl update to fixed versions.
	if (navigator.platform == 'MacIntel'
			&& navigator.userAgent.search('Chrome') != -1) {
		alert('Due to a bug in Chrome on MacOS this page does not work - please use Safari or Firefox');
		return true;
	} else {
		return false;
	}
}

function deselect() {
	var sel = window.getSelection ? window.getSelection() : document.selection;
	if (sel) {
		if (sel.removeAllRanges) {
			sel.removeAllRanges();
		} else if (sel.empty) {
			sel.empty();
		}
	}
}

// overflow-x: hidden; width: 600px;
// overflow-y: auto; height: 600px;

function senddone(oid) {
	var http = new XMLHttpRequest();
	http.open("get", "/admin/?cmd=done&oid=" + oid, true);
	http.onreadystatechange = function() {
		if (http.readyState == 4) {
			if (http.status != 200)
				alert(http.statusText);
			else
				document.getElementById(oid).style.color = 'green';
		}
	};
	http.send();
}

function removeElementById(el) {
	if (document.getElementById(el)) {
		var pxr = document.getElementById(el);
		par = pxr.parentNode;
		par.removeChild(pxr);
		delete pxr;
	}
}

function ratesdiv() {
	var tdiv = document.getElementById('teur');
	var europrice = parseFloat(tdiv.innerHTML);
	var ret = '';
	// assert(ret);
	var sa = [];
	for (i in rates) {
		sa.push(i);
	}
	for (i in sa.sort()) {
		var ep = '' + (europrice * rates[sa[i]]);
		var ind = ep.indexOf('\.');
		ret += sa[i] + ': ' + ep.substring(0, ind + 3) + '<br>';
		// assert( i, ep.substring(0, ind + 3));

	}
	ret += rtime;
	var rd = document.createElement("div");
	rd.innerHTML = ret;
	rd.style.fontSize = '12px';
	rd.style.overflowY = 'scroll';
	rd.style.width = '120px';
	rd.style.height = '70px';
	var rrr = document.getElementById('rrr');
	rrr.appendChild(rd);
}

// LZW Compression/Decompression for Strings
function compress(uncompressed) {
	"use strict";
	var i, dictionary = {}, c, wc, w = "", result = [], dictSize = 256;
	for (i = 0; i < 256; i += 1) {
		dictionary[String.fromCharCode(i)] = i;
	}
	for (i = 0; i < uncompressed.length; i += 1) {
		c = uncompressed.charAt(i);
		wc = w + c;
		if (dictionary[wc]) {
			w = wc;
		} else {
			result.push(dictionary[w]);
			dictionary[wc] = dictSize++;
			w = String(c);
		}
	}
	if (w !== "") {
		result.push(dictionary[w]);
	}
	return result;
}

function setInner(ele, txt) {
	document.getElementById(ele).innerHTML = txt;
}

function getVal(ele) {
	var a = document.getElementById(ele) ? document.getElementById(ele).value
			: 1;
	// alert('getval ' + ele + ", " + a);
	return a;
}

function setVal(ele, val) {
	return document.getElementById(ele).value = val;
}

function img2CanvasResize(img, w, h) {
	var can = document.createElement("canvas");
	can.id = "rcan";
	can.width = w;
	can.height = h;
	var context = can.getContext("2d");
	context.drawImage(img, 0, 0, img.width, img.height, 0, 0, w, h);
	return can;
}

function img2Canvas(img) {
	var can = document.createElement("canvas");
	can.width = img.width;
	can.height = img.height;
	var context = can.getContext("2d");
	context.drawImage(img, 0, 0, img.width, img.height);
	return can;
}

function drawspectrum(can) {
	var w = can.width;
	var h = can.height;
	var ctx = can.getContext("2d");
	var d = ctx.getImageData(0, 0, can.width, can.height);
	var data = d.data;
	var spectrum = new Array(256);
	var max = 1;
	for ( var i = 0; i < 256; i++) {
		spectrum[i] = 0;
	}

	var p = can.width * can.height * 4;
	var i = 0;
	var wcnt = 0;
	while (i < p) {
		x = parseInt((data[i] + data[i + 1] + data[i + 2]) / 3);
		if (data[i + 3] == 0) {
			x = 255;
		}
		spectrum[x] = spectrum[x] + 1;
		max = Math.max(max, spectrum[x]);
		i += 4;
	}
	// assert(true, spectrum[255], spectrum[254], spectrum[253] );
	ctx.fillStyle = "rgba(0, 255, 255, .3)";
	var barwidth = w / 258.0;
	pos = barwidth;
	for ( var i = 0; i < 256; i++) {
		var l = h / max * spectrum[i];
		ctx.fillRect(pos, h - l, barwidth, l);
		pos += barwidth;
	}
}

function convertBW(can) {
	/*
	 * two kinds of input: bw or color bw gets spread and color converted to
	 * greyscale do both in one run and decide wich one to take in the end
	 * 
	 */
	var context = can.getContext("2d");
	var d = context.getImageData(0, 0, can.width, can.height);
	var data = d.data;
	var p = can.width * can.height * 4;
	var data2 = new Array(p);
	var i = 0;
	var bwcnt = 0;
	while (i < p) {
		// color conv
		if (data[i + 3] < 3) { // alfa near zero means do not show - means
								// white for engraving
			x = 255;
			y = 255;
			data[i + 3] = 0;
		} else {
			y = (data[i] + data[i + 1] + data[i + 2]) / 3;
			if (y > 253) {
				data[i + 3] = 0;
			} else {
				x = parseInt(0.21 * data[i] + 0.71 * data[i + 1] + 0.07
						* data[i + 2]);
			}
		}
		data[i] = x;
		data[i + 1] = x;
		data[i + 2] = x;

		// bw spread
		if (y < 5) { // black
			y = 0;
			bwcnt += 1.0;
			data2[i] = y;
			data2[i + 1] = y;
			data2[i + 2] = y;
			data2[i + 3] = 255;
		} else if (y > 250) { // white
			y = 255;
			bwcnt += 1.0;
			data2[i] = y;
			data2[i + 1] = y;
			data2[i + 2] = y;
			data2[i + 3] = 0;
		} else {
			data2[i] = y;
			data2[i + 1] = y;
			data2[i + 2] = y;
			data2[i + 3] = 255;
		}
		i += 4;
	}
	if ((p / 4) / bwcnt > 1.1) { // if majority is color
		context.putImageData(d, 0, 0);
		// assert(true,'converted color', p / 4.0 , bwcnt);
	} else {
		i = 0;
		while (i < p) {
			data[i] = data2[i];
			i++;
		}
		context.putImageData(d, 0, 0);
		// assert(true,'converted BW', p / 4.0 , bwcnt);
	}
	// drawspectrum(can);
	return can;
}

function postURL(url, obj, respFunc) {
	var http = new XMLHttpRequest();
	var params = "s_json=" + JSON.stringify(obj);
	//assert(true, 'post', url, params);
	http.open("post", url, true); // open(method, url , async)
	http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http.setRequestHeader("Content-length", params.length);
	http.setRequestHeader("Connection", "close");
	http.onreadystatechange = function() {
		if (http.readyState == 4) {
			if (http.status != 200)
				assert(false, http.statusText);
			else if (respFunc)
				respFunc(http.responseText);
		}
	};
	http.send(params);
}

function getURL(url, respFunc) {
	var http = new XMLHttpRequest();
	http.open("get", url, true);
	//assert(true, 'get', url);
	http.onreadystatechange = function() {
		if (http.readyState == 4) {
			if (http.status != 200)
				assert(false, http.statusText);
			else if (respFunc)
				respFunc(http.responseText);
		}
	};
	http.send(null);
}


/**
*
*  Secure Hash Algorithm (SHA256)
*  http://www.webtoolkit.info/
*
*  Original code by Angel Marin, Paul Johnston.
*
*  and in Python:
*       import hashlib
*        hashval = hashlib.sha256("plainmessage").hexdigest()
*
*
**/
 
function SHA256(s){
 
	var chrsz   = 8;
	var hexcase = 0;
 
	function safe_add (x, y) {
		var lsw = (x & 0xFFFF) + (y & 0xFFFF);
		var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
		return (msw << 16) | (lsw & 0xFFFF);
	}
 
	function S (X, n) { return ( X >>> n ) | (X << (32 - n)); }
	function R (X, n) { return ( X >>> n ); }
	function Ch(x, y, z) { return ((x & y) ^ ((~x) & z)); }
	function Maj(x, y, z) { return ((x & y) ^ (x & z) ^ (y & z)); }
	function Sigma0256(x) { return (S(x, 2) ^ S(x, 13) ^ S(x, 22)); }
	function Sigma1256(x) { return (S(x, 6) ^ S(x, 11) ^ S(x, 25)); }
	function Gamma0256(x) { return (S(x, 7) ^ S(x, 18) ^ R(x, 3)); }
	function Gamma1256(x) { return (S(x, 17) ^ S(x, 19) ^ R(x, 10)); }
 
	function core_sha256 (m, l) {
		var K = new Array(0x428A2F98, 0x71374491, 0xB5C0FBCF, 0xE9B5DBA5, 0x3956C25B, 0x59F111F1, 0x923F82A4, 0xAB1C5ED5, 0xD807AA98, 0x12835B01, 0x243185BE, 0x550C7DC3, 0x72BE5D74, 0x80DEB1FE, 0x9BDC06A7, 0xC19BF174, 0xE49B69C1, 0xEFBE4786, 0xFC19DC6, 0x240CA1CC, 0x2DE92C6F, 0x4A7484AA, 0x5CB0A9DC, 0x76F988DA, 0x983E5152, 0xA831C66D, 0xB00327C8, 0xBF597FC7, 0xC6E00BF3, 0xD5A79147, 0x6CA6351, 0x14292967, 0x27B70A85, 0x2E1B2138, 0x4D2C6DFC, 0x53380D13, 0x650A7354, 0x766A0ABB, 0x81C2C92E, 0x92722C85, 0xA2BFE8A1, 0xA81A664B, 0xC24B8B70, 0xC76C51A3, 0xD192E819, 0xD6990624, 0xF40E3585, 0x106AA070, 0x19A4C116, 0x1E376C08, 0x2748774C, 0x34B0BCB5, 0x391C0CB3, 0x4ED8AA4A, 0x5B9CCA4F, 0x682E6FF3, 0x748F82EE, 0x78A5636F, 0x84C87814, 0x8CC70208, 0x90BEFFFA, 0xA4506CEB, 0xBEF9A3F7, 0xC67178F2);
		var HASH = new Array(0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A, 0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19);
		var W = new Array(64);
		var a, b, c, d, e, f, g, h, i, j;
		var T1, T2;
 
		m[l >> 5] |= 0x80 << (24 - l % 32);
		m[((l + 64 >> 9) << 4) + 15] = l;
 
		for ( var i = 0; i<m.length; i+=16 ) {
			a = HASH[0];
			b = HASH[1];
			c = HASH[2];
			d = HASH[3];
			e = HASH[4];
			f = HASH[5];
			g = HASH[6];
			h = HASH[7];
 
			for ( var j = 0; j<64; j++) {
				if (j < 16) W[j] = m[j + i];
				else W[j] = safe_add(safe_add(safe_add(Gamma1256(W[j - 2]), W[j - 7]), Gamma0256(W[j - 15])), W[j - 16]);
 
				T1 = safe_add(safe_add(safe_add(safe_add(h, Sigma1256(e)), Ch(e, f, g)), K[j]), W[j]);
				T2 = safe_add(Sigma0256(a), Maj(a, b, c));
 
				h = g;
				g = f;
				f = e;
				e = safe_add(d, T1);
				d = c;
				c = b;
				b = a;
				a = safe_add(T1, T2);
			}
 
			HASH[0] = safe_add(a, HASH[0]);
			HASH[1] = safe_add(b, HASH[1]);
			HASH[2] = safe_add(c, HASH[2]);
			HASH[3] = safe_add(d, HASH[3]);
			HASH[4] = safe_add(e, HASH[4]);
			HASH[5] = safe_add(f, HASH[5]);
			HASH[6] = safe_add(g, HASH[6]);
			HASH[7] = safe_add(h, HASH[7]);
		}
		return HASH;
	}
 
	function str2binb (str) {
		var bin = Array();
		var mask = (1 << chrsz) - 1;
		for(var i = 0; i < str.length * chrsz; i += chrsz) {
			bin[i>>5] |= (str.charCodeAt(i / chrsz) & mask) << (24 - i%32);
		}
		return bin;
	}
 
	function Utf8Encode(string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";
 
		for (var n = 0; n < string.length; n++) {
 
			var c = string.charCodeAt(n);
 
			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}
 
		}
 
		return utftext;
	}
 
	function binb2hex (binarray) {
		var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
		var str = "";
		for(var i = 0; i < binarray.length * 4; i++) {
			str += hex_tab.charAt((binarray[i>>2] >> ((3 - i%4)*8+4)) & 0xF) +
			hex_tab.charAt((binarray[i>>2] >> ((3 - i%4)*8  )) & 0xF);
		}
		return str;
	}
 
	s = Utf8Encode(s);
	return binb2hex(core_sha256(str2binb(s), s.length * chrsz));
 
}

function entrans(input){
	output = input.replace(/\+/g,"_1");
	output = output.replace(/\//g,"_2");
	output = output.replace(/=/g,"_3");
	return output;
}
function detrans(input){
	output = input.replace(/_1/g,"+");
	output = output.replace(/_2/g,"/");
	output = output.replace(/_3/g,"=");
	return output;
}


/**
*
*  Base64 encode / decode
*  http://www.webtoolkit.info/
*
**/
var Base64 = {

// private property
_keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

// public method for encoding
encode : function (input) {
    var output = "";
    var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
    var i = 0;

    input = Base64._utf8_encode(input);

    while (i < input.length) {

        chr1 = input.charCodeAt(i++);
        chr2 = input.charCodeAt(i++);
        chr3 = input.charCodeAt(i++);

        enc1 = chr1 >> 2;
        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
        enc4 = chr3 & 63;

        if (isNaN(chr2)) {
            enc3 = enc4 = 64;
        } else if (isNaN(chr3)) {
            enc4 = 64;
        }

        output = output +
        this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
        this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

        if(i % 60 == 0) output = output + '\n';
    }

    return output;
},

// public method for decoding
decode : function (input) {
    var output = "";
    var chr1, chr2, chr3;
    var enc1, enc2, enc3, enc4;
    var i = 0;

    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
    
    while (i < input.length) {

        enc1 = this._keyStr.indexOf(input.charAt(i++));
        enc2 = this._keyStr.indexOf(input.charAt(i++));
        enc3 = this._keyStr.indexOf(input.charAt(i++));
        enc4 = this._keyStr.indexOf(input.charAt(i++));

        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;

        output = output + String.fromCharCode(chr1);

        if (enc3 != 64) {
            output = output + String.fromCharCode(chr2);
        }
        if (enc4 != 64) {
            output = output + String.fromCharCode(chr3);
        }

    }

    output = Base64._utf8_decode(output);

    return output;

},

// private method for UTF-8 encoding
_utf8_encode : function (inp) {
	inp = inp.replace(/\r\n/g,"\n");
    var utftext = "";

    for (var n = 0; n < inp.length; n++) {

        var c = inp.charCodeAt(n);

        if (c < 128) {
            utftext += String.fromCharCode(c);
        }
        else if((c > 127) && (c < 2048)) {
            utftext += String.fromCharCode((c >> 6) | 192);
            utftext += String.fromCharCode((c & 63) | 128);
        }
        else {
            utftext += String.fromCharCode((c >> 12) | 224);
            utftext += String.fromCharCode(((c >> 6) & 63) | 128);
            utftext += String.fromCharCode((c & 63) | 128);
        }

    }

    return utftext;
},

// private method for UTF-8 decoding
_utf8_decode : function (utftext) {
    var inp = "";
    var i = 0;
    var c = c1 = c2 = 0;

    while ( i < utftext.length ) {

        c = utftext.charCodeAt(i);

        if (c < 128) {
        	inp += String.fromCharCode(c);
            i++;
        }
        else if((c > 191) && (c < 224)) {
            c2 = utftext.charCodeAt(i+1);
            inp += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
            i += 2;
        }
        else {
            c2 = utftext.charCodeAt(i+1);
            c3 = utftext.charCodeAt(i+2);
            inp += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
            i += 3;
        }

    }

    return inp;
}

};

