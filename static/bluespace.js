/*
 * 
 * 
LICENSE

This software is distributed under these license terms:

Copyright (c) 2016, Intrasts UG Rheurdt, Germany
All rights reserved.

For non commercial use the redistribution and use in source and binary forms, 
with or without modification, are permitted provided that the following 
conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intrasts UG Rheurdt nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL INTRASTS UG RHEURDT BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
THE COMMITTING OF SOURCES AND OR BINARIES TO THE BLITZER REPOSITORY DOES NOT TRANSFER
ANY RIGHTS TO THE COMMITTER. 

 */

var envname = "";  // the current env name
var envtext = "";  // the current text
var envAll = [];   // mapping of all name:text

var scriptname = "";  // same as above for scripts
var scripttext = "";
var scrAll = [];

var selected_apps = [];  // here the selected components get held
var scope = '_inst'; // '_comp' or '_node' for scope chooser buttons

var jobid = "-";
var runstate = 1;

var svr = document.location + '';

var stylename = 'blue'; // 'light' or 'dark'

function StyleSet(on){
	if(stylename == 'blue'){
		//assert2(stylename);
		document.body.style.color = '#cccccc';
		document.body.style.background = 'url(bluespace.jpg)';
		var els = document.getElementsByClassName('instance');
		for(var i in els ){
			if( els[i].style )
			els[i].style.background = '#aaaaaa';
		}
	}
	if(stylename == 'light'){
		document.body.style.color = '#202020';
		document.body.style.background = '#808080';
		var els = document.getElementsByClassName('instance');
		for(var i in els ){
			if( els[i].style )
			els[i].style.background = '#202020';
		}
	}
	if(stylename == 'dark'){
		document.body.style.color = '#808080';
		document.body.style.background = '#202020';
		var els = document.getElementsByClassName('instance');
		for(var i in els ){
			if( els[i].style )
			els[i].style.background = '#808080';
		}
	}
	if(on) LogoFlash();
}

function EnvNew(typ){
	var ename = prompt('Enter name for new ' + ( typ == 'env' ? 'environment:': 'script:' ) , 'x' );
	if(ename){
		if(typ == 'env'){
			if( ! envAll.hasOwnProperty( ename ) ){
				envname = ename;
				envtext = "";
			}
		} else{
			if( ! scrAll.hasOwnProperty( ename ) ){
				scriptname = ename;
				scripttext = "";
			}
		}
		EnvEdit(ename, typ);
	} 
}
function EnvSave(ename, typ){
	//assert('save');
	//var textval = document.getElementById('editor').value;
	var textval = window.editor.getValue();
	assert( textval)
	if(typ == 'env'){
		envtext = textval;
		envAll[ename] = textval;
		var x = { "command": "save", "typ": typ, "name": ename, "text": textval };
		WsSend( JSON.stringify(x) );
		MatrixShow( ename, Lines2Instances(envtext) );
	} else{
		scripttext = textval;
		scrAll[ename] = textval;
		var x = { "command": "save", "typ": typ, "name": ename, "text": textval };
		WsSend( JSON.stringify(x) );
		ScriptDo(ename, typ)
	}
}
function EnvDel(ename, typ){
	if (confirm('Are you sure you want to delete ' + ename + '?')) {
		var x = { "command": "del", "typ": typ, "envname": ename };
		WsSend( JSON.stringify(x) );
		if(typ == 'env'){
			delete envAll[ ename ];
			envtext = "";
			envname = "";
			removeElementById('zone');
		} else{
			document.getElementById('s_d').innerHTML = '<i title="script" class="fa fa-file-text "></i> ';
			delete scrAll[ ename ];
			scripttext = "";
			scriptname = "";
		}
		removeElementById('frameeditor');
	}
}
function EnvGet(ename, typ, callback){
	if(typ == 'env'){
		if( envname == ename ){
			MatrixShow_cont( envname, Lines2Instances(envtext) );
		} else{
			scope = '_inst';
			envname = ename;
			envtext = envAll[ ename ];
			MatrixShow( envname, Lines2Instances(envtext) );
			selected_apps = [];
		}
		
	} else {
		scriptname = ename;
		scripttext = scrAll[ scriptname ];
		ScriptDo(scriptname, typ);
	}
}
function EnvGetAll(){
	var x = { "command": "getall" };
	WsSend( JSON.stringify(x) );
}
function EditorResize(){
	var fe = document.getElementById('frameeditor');
	var ic = document.getElementById('edres');
	if( fe.style.width[0] == '6' ){
		fe.style.width = '33%';
		fe.style.zIndex = '6';
		ic.setAttribute('class','fa fa-plus-circle');
	} else{
		fe.style.width = '65%';
		fe.style.zIndex = '30';
		ic.setAttribute('class','fa fa-minus-circle');
	}
	editor.resize();
}
function EnvEdit(en,typ){
	var de = document.getElementById('editor');
	if(document.getElementById('editor')) removeElementById('frameeditor');
	var e = document.createElement('div')
	e.setAttribute('id','frameeditor');
	e.setAttribute('class','editor daim black');
	e.style.width = '32%';
	e.style.left = '33%';
	e.style.height = '97%';
	e.style.top = '1%';
	e.style.fontSize = '16px';
	var zone = document.getElementById('zone');
	if(zone) zone.style.width = '33%';
	e.style.zIndex='20';
	
//	editor.destroy();
//    editor.container.remove();
	var ret = eac('div','',
			  eac('div','title="close esc" onClick="removeElementById(\'frameeditor\'); var zone = document.getElementById(\'zone\'); zone.style.width = \'66%\';" style="cursor: pointer; float: left;"', '<i title="close" class="fa fa-close"></i>')
 		    + eac('div','title="resize editor" onClick="EditorResize();" style="cursor: pointer; float: left;"', '<i id="edres" class="fa fa-plus-circle"></i>')
			+ eac('div','title="delete" onClick="EnvDel(\'' + en + '\', \'' 
					+ typ + '\' );" style="cursor: pointer; float: left;"', '<i title="delete" class="fa fa-trash-o"></i>')
			+ eac('div','title="save ctrl-s" onClick="EnvSave(\'' + en + '\', \'' 
					+ typ + '\');" style="cursor: pointer; float: left;"', '<i class="fa fa-floppy-o"></i>')
//			+ ( typ == 'env' ? '' : eac('span','id="startstop" onClick="ScriptExec(\'' 
//					+ envname + '\',\'' + scriptname + '\')" style="cursor: pointer; "'
//					, '<i  title="run single!" class="fa fa-play-circle"></i>' ) )
			+ eac('span','style=" font-family: roboto, helvetica; font-weight: 600; padding-left: 10px; float: left;"',
					(typ == 'env' ? envname : scriptname))
			+ '<br>'
			+ eac('div','id="editor" title="edit" ', 
					(typ == 'env' ? envtext : scripttext) )
			);
	
	e.innerHTML = ret;
	document.body.appendChild(e);
	
    editor = ace.edit("editor");
    editor.setOptions({
    	  fontFamily: "courier",
    	  fontSize: "13px"
    	});
    editor.$blockScrolling = Infinity;
    editor.setTheme("ace/theme/xcode");
    editor.getSession().setMode("ace/mode/sh"); //.setUseWorker(false);
    //editor.setValue((typ == 'env' ? envtext : scripttext)); // or session.setValue
    editor.resize();
    editor.commands.addCommand({
        name: 'save',
        bindKey: {win: 'Ctrl-S',  mac: 'Command-S'},
        exec: function(editor) {
            //alert('saving you may');
            EnvSave(en, typ);
        },
        readOnly: true // false if this command should not apply in readOnly mode
    });
    editor.commands.addCommand({
        name: 'exit',
        bindKey: {win: 'ESC',  mac: 'ESC'},
        exec: function(editor) {
        	removeElementById('frameeditor'); 
        	var zone = document.getElementById('zone'); 
        	zone.style.width = '66%';
        },
        readOnly: true // false if this command should not apply in readOnly mode
    });

}
function InstanceGet(nci){
	//assert(node,app)
	var x = { "command": "getinstance", "jobid": jobid, "nci": nci };
	WsSend( JSON.stringify(x) );
}

function RunsClear(){
	clearasserts();
	StyleSet();
	var x = { "command": "clear" };
	WsSend( JSON.stringify(x) );
}
function ScriptDo(scr, typ){
	//assert2(0,scr);
	//var typ = 'Script';
	var el = document.getElementById('s_d');
	if(el){
		var ret = eac('span',' onClick="EnvEdit(\'' + scr + '\', \'' 
				+ typ + '\')" style="cursor: pointer;"'
				, '<i title="script" class="fa fa-file-text "></i> ' 
				+ scr + '<i title="edit script" class="fa fa-pencil black "></i>' )
			+ eac('span','id="startstop" onClick="ScriptExec(\'' 
					+ envname + '\',\'' + scr + '\')" style="cursor: pointer; "'
				, '<i  title="run!" class="fa fa-play-circle"></i>' )
			+ eac('select', 'style="font-size: 12px;" title="run mode" id="runmode"', ''
				+ eac('option', 'SELECTED','single')
				+ eac('option', '','parallel')
				+ eac('option', '','sequential')
				+ eac('option', '','2/3 available')
				+ eac('option', '','dist global linear')
				+ eac('option', '','dist global random')
				+ eac('option', '','dist component linear')
				+ eac('option', '','dist component random'))
		    + eac('span','onClick="RunsClear();" style="cursor: pointer;"'
				, '<i title="erase output" class="fa fa-eraser"></i>' );
		el.innerHTML = ret;
	}
}

function SelectionRemove(){
    if( selected_apps.length == 0 ){
    	return;
    }
    else{
    	var apps = document.getElementsByClassName('app');
        for(var i in apps){
        	if( typeof(apps[i]) != 'function' && typeof(apps[i]) != "number" ){
    			apps[i].parentNode.style.background='none';
        	}
        }
        selected_apps = [];
    }
    setTimeout("window.getSelection().removeAllRanges()", 20);
}
function SelectionInvert(){
	var apps = document.getElementsByClassName('app');
    if( selected_apps.length == 0 ){
        for(var i in apps){
        	if( typeof(apps[i]) != 'function' && typeof(apps[i]) != "number" ){
        		var appname = apps[i].parentNode.id.split('_')[1];
        		apps[i].parentNode.style.background='white';
        		selected_apps.push( appname )
        	}
        }
    }
    else{
        var newlist = [];
        for(var i in apps){
        	if( typeof(apps[i]) != 'function' && typeof(apps[i]) != "number" ){
        		var appname = apps[i].parentNode.id.split('_')[1];
        		if( inlist( appname, selected_apps )){
        			apps[i].parentNode.style.background='none';
        		}else{
        			newlist.push(appname)
        			apps[i].parentNode.style.background='white';
        		}
        	}
        }
        selected_apps = newlist;
    }
    setTimeout("window.getSelection().removeAllRanges()", 20);
}
function SelectionToggle(appname){
	//if(selected_apps.includes(appname)){ // in case the browser is recent as of Q2 2016....
	if( inlist(appname, selected_apps) ){ // in case the browser is a bit older....
		selected_apps.splice( selected_apps.indexOf(appname) ,1);
		document.getElementById('sss_'+appname).style.background='none';
	}
	else{
		selected_apps.push(appname);
		document.getElementById('sss_'+appname).style.background='white';
	}
	setTimeout("window.getSelection().removeAllRanges()", 20);
}
function LineTupleTest(){
	var a = Lines2Instances('server_[1-2]=appX,app[C,D,E]');
	for(var i in a){
		assert2(false,a[i])
	}
}
function Lines2Instances(txt){
	// expects a string with lines separated by '\n' newlines and returns a list of instances
	var ret = [];
	var all = txt.split('\n');
	if( scope == '_node' ){
		for(var i in all){
			var l = all[i];
			if( l.length > 3 && l[0] != '#' && l.indexOf('=') > 0 ){
				var svr = LineTuple(l.split('=')[0]);
				for(var a in svr){
					ret.push( [ svr[a] , 'nodes', '_' ] )
				}
			}
		}
	}
	else if( scope == '_inst' ){
		for(var i in all){
			var l = all[i];
			if( l.length > 3 && l[0] != '#' && l.indexOf('=') > 0 ){
				var svr = LineTuple(l.split('=')[0]);
				var apps = l.split('=')[1].split(';')
				for(var a in apps){
					var app = apps[a];
					var appins = LineTuple(app);
					for( var xa in appins ){
						for( var s in svr ){
							if( appins[xa].indexOf('|@') > -1 ){
								var nodenum = parseInt(svr[s].replace( /\D+/,'' ));
								if(nodenum > 26 ) nodenum = 0;
								var comp = appins[xa].split('|')[0];
								ret.push( [ svr[s] , comp, appins[xa].replace( /\|\@/, String.fromCharCode( 64 + nodenum ) ) ] )
							}
							else{
								ret.push( [ svr[s] , appins[xa], appins[xa] ] )
							}
						}
					}
				}
			}
		}
	}
	else if( scope == '_comp' ){
		var u = {};
		for(var i in all){
			var l = all[i];
			if( l.length > 3 && l[0] != '#' && l.indexOf('=') > 0 ){
				//var svr = LineTuple(l.split('=')[0]);
				var apps = l.split('=')[1].split(';')
				for(var a in apps){
					var comp = apps[a].split('|')[0]
					u[comp] = 1;
				}
				
			}
		}
		for( var i in u ){
			ret.push( [ '_',i,'_' ] );
		}
	}
	return ret;
}
function LineTuple(line){
	// here each line with [1-3] [A-Z] [1,s,d,555,sdf,2] gets expanded
	var ret = [];
	var re1 = /\[\w*-\w*\]/;
	var re2 = /\[[,\w*]*\]/; 
	//if( line.indexOf('[') > -1 && line.indexOf(']') > -1 && line.indexOf('-') > -1 ){
	if( re1.test( line ) ){
		var ante = line.split('[')[0];
		var rng = line.split('[')[1].split(']')[0];
		var post = line.substr( 1 + line.indexOf(']'));
		var first = rng.split('-')[0];
		var padZero = false;
		if( first[0] == '0' ) padZero = true;
		var last = rng.split('-')[1]
		//var repl = ante + '@' + post
		var c0, cN = 0;
		if(  (! isNaN(parseInt(first))) && (! isNaN(parseInt(last))) ){
			var c0 = parseInt(first);
			var cN = parseInt(last);
			for(var i = c0; i <= cN; i++ ){
				var zpad = '';
				if( i < 10 && padZero ) zpad = '0';
				var xline = ante + zpad + i + post;
				if( xline.indexOf(']') == -1 ){
					ret.push(xline)
				} else{
					ret = ret.concat( LineTuple(xline) );
				}
			}
		} else {
			var c0 = first.charCodeAt(0);
			var cN = last.charCodeAt(0);
			for(var i = c0; i <= cN; i++ ){
				var xline = ante + String.fromCharCode(i) + post;
				if( xline.indexOf(']') == -1 ){
					ret.push(xline)
				} else{
					ret = ret.concat( LineTuple(xline));
				}
			}
		}
	} else if (re2.test( line )) {
		var ante = line.split('[')[0];
		var rng = line.split('[')[1].split(']')[0].split(',');
		var post = line.substr( 1 + line.indexOf(']'));
		for(var i = 0; i < rng.length; i++){
			var xline = ante + rng[i] + post;
			if( xline.indexOf(']') == -1 ){
				ret.push(xline)
			} else{
				ret = ret.concat( LineTuple(xline));
			}
		}
	} else{
		ret.push( line )
	}
	//console.log(ret)
	return ret;
}
function ScopeSet(sc){
	if( sc == scope ) return;
	scope = sc;
	if(sc == '_inst'){
		document.getElementById( '_inst' ).setAttribute('class','butt daim');
		document.getElementById( '_comp' ).setAttribute('class','buttp daim');
		document.getElementById( '_node' ).setAttribute('class','buttp daim');
	}
	else if(sc == '_comp'){
		document.getElementById( '_inst' ).setAttribute('class','buttp daim');
		document.getElementById( '_comp' ).setAttribute('class','butt daim');
		document.getElementById( '_node' ).setAttribute('class','buttp daim');
	}
	else { // if(sc == '_node')
		document.getElementById( '_inst' ).setAttribute('class','buttp daim');
		document.getElementById( '_comp' ).setAttribute('class','buttp daim');
		document.getElementById( '_node' ).setAttribute('class','butt daim');
	}
	MatrixShow_cont( envname, Lines2Instances(envtext) );
}
function MatrixShow_head(en, instances){ 
	//envname = en
	removeElementById("zone");
	var z = document.createElement('div');
	var typ = 'env';
	z.setAttribute('class','zone');
	z.setAttribute('id','zone');
	if(document.getElementById('editor')) z.style.width='33%';
	var head = eac('div','class="zonehead daim"',
				eac('div','',  ''
				// + ( i > 0 ? '<i class="fa fa-trash black right "></i>' : '' )
				//+ '<i class="fa fa-copy black right "></i>' 
				+ eac('span','onClick="EnvEdit(\'' + en + '\', \'' + typ + '\')" style="cursor: pointer;"'
						, eac('i','title="environment" class="fa fa-server"','') + en + '<i title="edit environment" class="fa fa-pencil black "></i>' )
				+ eac('span','onClick="SelectionInvert();" title="invert selection" style="cursor: pointer; font-size: 1.3em; color: black;"'
						,'&#9703;' )
				+ eac('span','onClick="SelectionRemove();" title="remove selection" style="cursor: pointer; font-size: 1.3em; color: black;"'
						,'&#9707;' )
				+ eac('div','id="_node" onClick="ScopeSet(\'_node\');" class="butt'+( scope == '_node'? '':'p' )+' daim" title="run for each node"'
						,'node' )
				+ eac('div','id="_comp" onClick="ScopeSet(\'_comp\');" class="butt'+( scope == '_comp'? '':'p' )+' daim" title="run for each component"'
						,'comp' )
				+ eac('div','id="_inst" onClick="ScopeSet(\'_inst\');" class="butt'+( scope == '_inst'? '':'p' )+' daim" title="run for each instance"'
						,'inst' )
				) );
	head += eac('div','id="s_d" class="zonehead daim"','<i title="script" class="fa fa-file-text"></i>');
				/* '<i class="fa fa-unlock "></i>'
				+ '<i class="fa fa-circle-o-notch xfa-spin"></i>'
				+  eac('span','onClick="EnvEdit(\'' + envname + '\')" style="cursor: pointer;"'
						,'exec' )); */
	//assert2(txt);
	z.innerHTML = head + eac('div','id="matrix"','');
	document.body.appendChild(z);
}
function getKeys(map){
	var ki = map.keys();
	var ret = [];
	var nk = ki.next();
	while( nk.value ) {
		ret.push(nk.value);
		nk = ki.next();
	}
	return ret;
}

function MatrixShow_cont(en, instances){ 
	var app_map = new Map();
	for(var i in instances){
		var srv = instances[i][0];
		if( srv.split(';').length == 2 ) srv = srv.split(';')[1].trim();
		var app = instances[i][1].trim();
		var sl = app_map.get(app) || [];
		sl.push(srv + ';' +  instances[i][2] );
		app_map.set(app,sl);
	}
	var tuples = instances.length;
	// calc square size
	var screenra = 0.4;
	var ww = window.innerWidth;
	var wh = window.innerHeight;
	//assert(screenra, tuples,ww,wh)
	var pixels = ww * wh;
	var sqt = Math.sqrt(tuples);
	var pixelsr = (ww - sqt) * (wh - sqt);
	//assert( sqt, pixels, pixelsr )
	var dsize = Math.min(24, Math.max( 1, Math.floor( Math.sqrt( 
				ww * wh * screenra / tuples ))));
	var tw = Math.ceil(120.0 / (dsize + 1)) * (dsize + 1) ;
	var tab = '<table border="0">';
	var keylist = getKeys(app_map);
	keylist.sort();
	for( var i in keylist ){
		var comp = keylist[i];
		//console.log('Ms ', k);
		var v = app_map.get(comp);
	
	//app_map.forEach(function(v,k,m){  
		var row = '<tr><td id="sss_' + comp +'">' 
			+ eac('div','class="app" onClick="SelectionToggle(\''
					+ comp +'\');" style="float: left; width: ' 
					+ tw + 'px; overflow: hide;"', comp )
		for(var j=0; j < v.length; j++){
			var node_inst = v[j];
			var node = node_inst.split(';')[0];
			var inst = node_inst.split(';')[1];
			var nci = "";
			switch( scope ) {
				case '_node':
					nci = node;
					break;
				case '_comp':
					nci = comp;
					break;
				case '_inst':
					nci = node + ' ' + inst;
					break;
			}
			row += eac('div','onClick="InstanceGet(\'' + node + comp + inst + '\' );" class="instance" title="' 
					+ nci + '" id="' + node + comp + inst + '"' 
					+ ' style="width: ' + dsize  + 'px; height: ' + dsize  + 'px;"', ' ') //'s_' + j)	
		}
		row += '</td></tr>';
		tab += row;
	};	
	tab += '</table>';
	document.getElementById('matrix').innerHTML = eac('div','style="height: ' 
			+ (wh -120)  + 'px; overflow-y: auto;"',tab);
	StyleSet();
}
	
function MatrixShow(en, instances){ 
	MatrixShow_head(en, instances);
	MatrixShow_cont(en, instances);
}
function MenuDel(){
	removeElementById("glass");
	removeElementById("envmenu"); 
}
function PreViewShow(typ, ename){
	var dat = envAll;
	if(typ == 'Script'){ 
		dat = scrAll;
	}
	clearasserts();
	assert2(true,999, dat[ ename ]);
	assert2(true, 999, ename );
}
function Menu( xx, funcname, typ, pos ){
	removeElementById('envmenu');
	removeElementById('glass');
	var glass = document.createElement('div')
	glass.setAttribute('id','glass');
	glass.style.zIndex='5';
	glass.style.position='absolute';
	glass.style.width=(window.innerWidth -50 ) + 'px';
	glass.style.height=(window.innerHeight - 100 )+ 'px';
	glass.style.top='45px';
	glass.style.left='0px';
	glass.setAttribute('onMouseEnter','MenuDel();');
	document.body.appendChild(glass);
	var icon = "server";
	var menuname = "Environments";
	var menunames = o2l(envAll);
	if(typ == 'Script'){ 
		icon = 'file-text'
		menuname = 'Scripts'
		menunames = o2l(scrAll);
	}
	var ret = eac('div','class="menuent"',menuname);
	//envs = ['production','qs','integration','development','training','desaster'];
	for(var i in menunames){
		//console.log( menunames[i] )
		ret += eac('div','class="menuent"', 
	 			eac('div', 'onMouseEnter="PreViewShow( \'' + typ + '\', \'' + menunames[i] + '\' );" onClick="' + funcname + '(\'' + menunames[i] + '\',\'' + typ 
	 			+ '\'); MenuDel();"', '<i class="fa fa-' + icon +'"></i>&nbsp;' + menunames[i] ));
	}
	ret += eac('div','onClick="EnvNew(\'' + typ + '\'); MenuDel();" class="menuent"', '<i class="fa fa-plus-square green"></i> add new' );
	var em = document.createElement('div');
	em.setAttribute('id','envmenu');
	em.setAttribute('class','zonehead daim');
	em.style.zIndex='10';
	em.style.position="absolute";
	em.style.top='40px';
	em.style.left= (pos || 1) * 20 +'px';
	em.innerHTML = ret;
	document.body.appendChild(em);
	//em.style.display = 'inline-block';
}

function LogoFlash(){
	var dlay = 200;
	var ddd = 0;
	var reps = 10;
	for( var i = 0; i < reps; i++ ){
		setTimeout(	"document.getElementById('logo').src = 'Blitzer_y.png'", ddd );
		ddd += dlay;
		setTimeout(	"document.getElementById('logo').src = 'Blitzer.png'", ddd );
		ddd += dlay;
	}
}

function ScriptExec(envname,scriptname){
	// exec sx in ex in parallel / sequential
	//assert2(0,'exec', envname, scriptname, selected_apps)
	// in the flat data there is [node, comp, inst]
	
	if( runstate == 2 ){
		assert2(0,'execstop')
		runstate = 0;
		var x = { "command": "execstop", "jobid": jobid };
		WsSend( JSON.stringify(x) );
	} else if( runstate == 1 ){
		assert2(0,'exec')
		runstate = 0;
		jobid = new Date().toISOString();

		var flat = Lines2Instances( envtext );
		//assert2( envtext );
		if( selected_apps.length > 0 ){
			var ff = [];
			for(var i = 0; i < selected_apps.length; i++){
				var app = selected_apps[i];
				for(var f = 0; f < flat.length; f++){
					var fitem = flat[f];
					if( fitem[1] == app ) ff.push( fitem );
				}
			}
			flat = ff;
		}
		//console.log(flat);
		var oe = document.getElementById('runmode')
		var runmode = oe.options[ oe.selectedIndex ].text 
		var x = { "command": "exec", "jobid": jobid, "script": scriptname, "env": envname, 
			"flat": flat, "runmode": runmode };
		WsSend( JSON.stringify(x) );
	}
}

var wsu = svr.replace('http','ws').replace('static/blitzer','websocket');
//console.log( wsu );
var ws = new WebSocket(wsu);
function WsSend(msg){
	try{
		if( ws.readyState == ws.CLOSED ){
			 ws = new WebSocket(wsu);
			 assert2(0,'closed: new ws', wsu );
		}
		ws.send(msg);
	} catch(ex){
		 ws = new WebSocket(wsu);
		 assert2(0,'failed: new ws', wsu );
		//location.reload();
	}
}
ws.onopen = function() {
	//getEnvNames();
	EnvGetAll();
	LogoFlash();
};

ws.onmessage = function(evt) {
	
	//console.log(evt.data);

	var dat = JSON.parse(evt.data);
	
	if( dat['start'] ){
		var node = dat.start[0];
		var comp = dat.start[1];
		var inst = dat.start[2];
		if( comp == 'default_single' ){
			
		}else{
			document.getElementById( node + comp + inst ).style.background = 'blue';
		}
	}
	else if( dat['end'] ){
		var node = dat['end'][0];
		var comp = dat['end'][1];
		var inst = dat['end'][2];
		var id   = dat['end'][3];
		var ret  = dat['end'][4];
		var code = 0;
		if( comp == 'default_single' ){
			code = ret[0];
			assert2( true, 999, ret[1] )
			assert2( false, 999, ret[2] )
		} else{
			code = ret
			if( code == 0){
				document.getElementById( node + comp + inst ).style.background = 'green';
			} else {
				document.getElementById( node + comp + inst ).style.background = 'red';
			}
		}
	}
	else if( dat['execstate'] ){
		var state = dat['execstate'];
		var ji = dat['jobid'];
		if( ji == jobid && state == 'running' ){
			runstate = 2;
			assert('job running')
			document.getElementById('startstop').innerHTML = '<i title="stop!" class="fa fa-stop-circle red "</i>'
		}
		else if( ji == jobid && ( state == 'terminated' || state == 'finished' ) ){
			runstate = 1;
			assert('job state ', state)
			document.getElementById('startstop').innerHTML = '<i title="run!" class="fa fa-play-circle blue "></i>'
		}
	}
	else if( dat['getinstance'] ){
		clearasserts();
		//console.log(dat['out'])
		assert2(true, 999, dat['out'])
		assert2(true, dat['node'])
		assert2(false, 999, dat['err'])
		assert2(false, dat['node'])
	}
	else if( dat['envall'] ){
		envAll = dat['env_'];
		scrAll = dat['scr_'];
		envnames = dat['envnames'];
		scriptnames = dat['scrnames'];
	}

};

