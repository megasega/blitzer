Contributers welcome, no matter if you develop, test or simply use Blitzer and 
give feedback or file issues.

If you manage large infrastructures and like to try Blitzer, we
are happy to get you started.

For development solid python and javascript skills are of good use.
For coding style we like to keep it simple and functional and only use objects
where it makes sense and dont use Blitzer to apply the latest design patterns.

Maintainability first and minimal external dependencies from libs or frameworks.