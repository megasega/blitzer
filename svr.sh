#!/bin/bash
cd `dirname $0`
case $1 in
	on)
		if [ ! -f svr.pid ]; then
			#nohup python3 -m http.server 8888 1>/dev/null 2>&1 &
			nohup python -m SimpleHTTPServer 8888 1>/dev/null 2>&1 &
			echo $! > svr.pid
		fi
	;;
	off)
		if [ -f svr.pid ]; then
			kill -9 `cat svr.pid`
			rm svr.pid
		fi
	;;
	*)
		echo "use with on or off!"
	;;
esac
