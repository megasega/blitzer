#!/usr/bin/python
VERSION="0.1.44"
LICENSE = """

This software is distributed under these license terms:

Copyright (c) 2016, Intrasts UG Rheurdt, Germany
All rights reserved.

For non commercial use the redistribution and use in source and binary forms, 
with or without modification, are permitted provided that the following 
conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intrasts UG Rheurdt nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL INTRASTS UG RHEURDT BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
THE COMMITTING OF SOURCES AND OR BINARIES TO THE BLITZER REPOSITORY DOES NOT TRANSFER
ANY RIGHTS TO THE COMMITTER. 
"""

import json #json.dumps json.loads
import requests
import os, sys, getopt
import math
from multiprocessing import Pool, Queue
from threading import Thread
from subprocess import Popen


import socket 
import time
from datetime import datetime
import glob
from random import randint, random
import traceback, sys

from gevent.pywsgi import WSGIServer
from geventwebsocket import WebSocketError
from geventwebsocket.handler import WebSocketHandler

# get-pip.py
# pip2.7 install gevent bottle

from base64 import b64encode
from bottle import request, Bottle, abort, static_file, redirect
from uuid import uuid4

counter = 0
jobthreads = {}
allscripts = {}
q = Queue()
ResultQueue = Queue()
CasJobQueue = Queue()

os.chdir( os.path.dirname(os.path.abspath(sys.argv[0])) )
datadir = os.getcwd()
workdir = '/tmp/blitzer/'
envroot = datadir + '/env/' 
scrroot = datadir + '/scr/' 
port = 8888
ip = socket.gethostbyname(socket.gethostname())
threads = 50
dist = 10
#print os.getenv('PYTHONPATH')
app = Bottle()

''' The Session store: simply a dict...'''
Session = {} # a dict of many token: session_dict

@app.route('/upload/<filename>')
def server_tmp(filename):
    #print filename
    return static_file(filename, root='./upload')

@app.route('/static/<filename>')
def server_static(filename):
    #print filename
    return static_file(filename, root='./static')

@app.route('/fonts/<filename>')
def server_fonts(filename):
    #print filename
    return static_file(filename, root='./static/fonts')

def seccheck():
    t = request.headers['Token'].encode('ascii','ignore')
    if not t:
        abort(400)
        return False; False
    else:
        if Session.has_key(t):
            dat = Session.pop(t)
            newt = str(uuid4())
            Session[newt] = dat
            return dat['email'], newt
        else:
            abort(400)

@app.route('/')        
def start():
    redirect("/static/blitzer")
    #return static_file('bluespace.html', root='./static/')
 
@app.route( '/auth/login', method='POST')
def login():
    for i in request.forms:
        print i
    if 'assertion' not in request.forms:
        abort(400)
    # Send the assertion to Mozilla's verifier service.
    data = {'assertion': request.forms.assertion, 'audience': 'http://localhost:8080'}
    #print request.form['assertion']
    resp = requests.post('https://verifier.login.persona.org/verify', data=data, verify=True)
    """ {"status": "okay",
          "email": "bob@eyedee.me",
          "audience": "https://example.com:443",
          "expires": 1308859352261,
          "issuer": "eyedee.me" }    """
    # Did the verifier respond?
    if resp.ok:
        # Parse the response
        verification_data = resp.json()
        #print localtime(int( verification_data['expires']) / 1000 )
        # Check if the assertion was valid
        if verification_data['status'] == 'okay':
            # Log the user in by setting a secure session cookie
            #session['email']= verification_data['email']
            newt = str(uuid4())
            Session[newt] = verification_data
            print 'logged in as:' + verification_data['email'], newt
            #session['token'] = active_token 
            return '{ "ok": "true", "token": "%s" }' % newt
            
    # Oops, something failed. Abort.
    abort(500)
    # , methods=['GET', 'POST']


@app.route('/auth/logout', method='POST')
def logout():
    t = request.headers['Token'].encode('ascii','ignore')
    print 'logout'
    if t and Session.has_key(t):
        Session.pop(t)
    return '{ "ok": "true" }'

def getdat( envname, typ ):
    global scrroot, envroot
    if typ == 'env':
        fp = envroot + envname 
    else:
        fp = scrroot + envname 
    if os.path.exists(fp):
        envfile = open( fp, 'r')
        text = envfile.read()
        envfile.close()
    return text

def getNames():
    ret = {}
    envpaths = glob.glob(envroot + '*')
    envnames = []
    for i in envpaths:
        envnames.append(i.split('/')[-1] ) 
    print envnames
    ret['envnames'] = envnames
    envpaths = glob.glob(scrroot + '*')
    scriptnames = []
    for i in envpaths:
        scriptnames.append(i.split('/')[-1] ) 
    print scriptnames
    ret['scriptnames'] = scriptnames
    ret['status'] = 'getenvnames ok'
    return ret

@app.route('/websocket')
def handle_websocket():
    global envroot, jobthreads, workdir, datadir, threads
    print 'new websocket'
    wsock = request.environ.get('wsgi.websocket')
    if not wsock:
        abort(400, 'Expected WebSocket request.')
    while True:
        try:
            message = wsock.receive()
            current_client = wsock.handler.active_client
            #print message
            if message is None: continue
            jm = json.loads(message)
            cmd = jm['command']
            #print cmd
            ret = {}
            if cmd == 'save':
                envname = jm['name']
                text = jm['text']
                typ = jm['typ']
                if typ == 'env':
                    envfile = open( envroot + envname, 'w')
                else:
                    envfile = open( scrroot + envname, 'w')
                envfile.write( text.encode('utf-8'))
                envfile.close()
                ret = getNames()
                ret['status'] = 'save ok'
            elif cmd == 'del':
                envname = jm['envname']
                typ = jm['typ']
                if typ == 'env':
                    fp = envroot + envname 
                else:
                    fp = scrroot + envname 
                if os.path.exists(fp):
                    os.remove(fp)
                    ret = getNames()
            elif cmd == 'getnames':
                ret = getNames()
            elif cmd == 'get':
                envname = jm['envname']
                typ = jm['typ']
                #print 'get',envname, typ
                if typ == 'env':
                    fp = envroot + envname 
                else:
                    fp = scrroot + envname 
                if os.path.exists(fp):
                    envfile = open( fp, 'r')
                    text = envfile.read()
                    envfile.close()
                    ret['env'] = envname
                    ret['text'] = text
                    ret['typ'] = typ
                    ret['status'] = 'get ok'
            elif cmd == 'getall':
                for nn in ['env','scr']:
                    lis = {}
                    nnnames = []
                    if nn == 'env':
                        fls = glob.glob(envroot + '*')
                    else:
                        fls = glob.glob(scrroot + '*')
                    for fl in fls:
                        ff = fl.split('/')[-1]
                        if os.path.exists(fl):
                            envfile = open( fl, 'r')
                            lis[ff] = envfile.read()
                            envfile.close()
                            nnnames.append(ff)
                    ret[ nn + 'names' ] = nnnames
                    ret[ nn + '_' ] = lis
                    #print lis, nnnames
                ret['envall'] = 'all'   
                ret['status'] = 'getall ok'
            elif cmd == 'getinstance':
                #var x = { "command": "getinstance", "jobid": jobid, "node": node, "app": app };
                jobid = jm['jobid']
                if jobid != '-':
                    nci = jm['nci']
                    #app = jm['app']
                    ret[ 'getinstance' ] = jobid
                    ret[ 'node' ] = nci
                    #ret[ 'app' ] = app
                    ret[ 'out' ] = get1MB( workdir + jobid + '/' + nci + '.out')
                    ret[ 'err' ] = get1MB( workdir + jobid + '/' + nci + '.err')
            elif cmd == 'clear':
                os.popen('rm -r ' + workdir +'*')
            elif cmd == 'execstop':
                jobid = jm['jobid']
                sf = open( workdir + 'stop_' + jobid, 'w')
                sf.write('stop')
                sf.close()
            elif cmd == 'exec':
                # we have an env and a script
                # now we create a task for each instance
                # put them in a queue and start a pool of worker threads
                # x = { "command": "exec", "jobid": jobid, "script": scriptname, "env": envname, "flat": flat }
                jobid = jm['jobid']
                script = jm['script']
                env = jm['env']
                flat = jm['flat']
                runmode = jm['runmode']
                if runmode == 'parallel':
                    print 'exec parallel', jobid, env, script, flat
                    wx = JobThread( runmode, jobid, script, env, flat, current_client, POOLSIZE=threads)
                    
                elif runmode == 'sequential':
                    print 'exec sequential', jobid, env, script, flat
                    wx = JobThread( runmode, jobid, script, env, flat, current_client, POOLSIZE=1 )
                
                elif runmode == '2/3 available':
                    print 'exec 2/3 available', jobid, env, script, flat
                    wx = JobThread( runmode, jobid, script, env, flat, current_client, POOLSIZE=threads, GROUPSIZE=3 )
                
                elif runmode == 'dist global linear':
                    print 'exec dist global', jobid, env, script, flat
                    wx = JobThread( runmode, jobid, script, env, flat, current_client, POOLSIZE=threads)
                elif runmode == 'dist global random':
                    print 'exec dist global', jobid, env, script, flat
                    wx = JobThread( runmode, jobid, script, env, flat, current_client, POOLSIZE=threads, rnd=True)
                    
                elif runmode == 'dist component linear':
                    print 'exec dist component', jobid, env, script, flat
                    wx = JobThread( runmode, jobid, script, env, flat, current_client, POOLSIZE=threads)
                elif runmode == 'dist component random':
                    print 'exec dist component', jobid, env, script, flat
                    wx = JobThread( runmode, jobid, script, env, flat, current_client, POOLSIZE=threads, rnd=True)
                    
                elif runmode == 'single':
                    print 'exec single', jobid, env, script, flat
                    flat = [['localhost','default_single', '_']]
                    wx = JobThread( runmode, jobid, script, env, flat, current_client, POOLSIZE=1 )

                wx.start()
                jobthreads[ jobid ] = ( wx, current_client.ws, )
                ret['execstate'] = 'running'
                ret['jobid'] = jobid

            current_client.ws.send( json.dumps(ret) )
        except Exception as e:
            #traceback.print_exception(Exception, e, sys.exc_info()[2])        
            print 'websock error! ' + repr(e)
            break

def get1MB(path):
    ret = path
    try:
        if os.path.exists(path):
            osof = open( path, 'r')
            osize = os.stat(path).st_size
            if osize > 1024*1024 :
                ret = osof.read(1024*1024) + '\n...showing 1MB of ' + str(int(osize / (1024*1024))) + 'MB'
            else:
                ret = osof.read()
            osof.close()
    except Exception as e:
        traceback.print_exception(Exception, e, sys.exc_info()[2])
        ret = sys.exc_info()[2]
    return ret

def runpopen(job, cmd, allback=False):
    # Popen(["/bin/mycmd", "myarg"], env={"PATH": "/usr/bin"})
    #p = Popen(["ssh", "localhost","ls -la /tmp"], bufsize=1)
    # -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null 
    # 
    #d = datetime.date.today()
    #dd = d.strftime("%Y%m%d_%M%S_%f")
    #print 'runpopen comp:', job.comp, 'cred:', job.cred, 'node:', job.node, 'source:',  job.source
    env = 'cred="%s"\nnode=%s\ncomp=%s\ninst=%s\nsource=%s\n' % ( job.cred, job.node, job.comp, job.inst, job.source)
    deb = '\necho $file\necho $comp.zip\n'
    #print cmd
    nci = job.node + job.comp + job.inst
    path = job.path + '/' + nci
    osof = open( path + '.out', 'a')
    osef = open( path + '.err', 'a')
    p = Popen( env + cmd, stdout=osof, stderr=osef, bufsize=1, shell=True)
    #p = Popen( env + cmd + deb, stdout=osof, stderr=osef, bufsize=1, shell=True)
    ret = p.wait()
    osof.close()
    osef.close()
    
    if not allback:
        return ret
    
    oout = get1MB(path + '.out')
    oerr = get1MB(path + '.err')

    #sout = cStringIO.StringIO()
    #print "pid:", p.pid, 'ret:',ret
    return (ret, oout, oerr)

                    
class QThread(Thread):
    """ thread to issue feedback to the client """
    def __init__(self, jobid, current_client):
        Thread.__init__(self) 
        self.jobid = jobid
        self.current_client = current_client
        
    def run(self): 
        global q
        while True:
            ret = q.get()
            if ret[1] == self.jobid and ret[0] == 'stop':
                break
            try:
                x = { ret[0]: [ ret[1], ret[2], ret[3], ret[4], ret[5] ] }
                self.current_client.ws.send( json.dumps(x) )
            except:
                print 'QThread send error:', ret
                

class Job():
    """ data structure to hold parameters for a job 
    """
    def __init__(self, jobid, path, node, comp, inst, source):
        self.jobid = jobid
        self.path = path
        self.comp = str(comp).strip()
        self.inst = str(inst).strip()
        self.source = source
        self.failed = 0
        self.done = False
        if str(node).find(';') > -1 :
            self.cred = node.split(';')[0].strip()
            self.node = node.split(';')[1].strip()
        else:
            self.cred = ''
            self.node = str(node).strip()
        #print self.cred, self.node, self.comp, self.inst
    
def do( job ):
    global allscripts, q, ResultQueue, workdir
    os.nice(10)
    if os.path.isfile(workdir + 'stop_' + job.jobid) : # omitting thread for job.jobid
        return
    #print 'new thread start for: ', jobid
    q.put( ('start', job.node, job.comp, job.inst, job.jobid, 0 ) )
    acmd = allscripts[ job.jobid ]
    
    # this avoids reset of connections to ssh when going through one ssh server
    time.sleep( random() )
    
    if job.source == '_': # execute modes
        if job.comp == 'default_single':
            ret = runpopen( job, acmd, allback=True)
        else:
            ret = runpopen( job, acmd, allback=False)
    else: # distribute
        acmd += "\n"
        ss = 'http://$source:8888/'
        if job.source.find( 'http' ) > -1:
            ss = job.source
        acmd += """cat svr.sh | ssh $cred@$node "umask 0077 ; mkdir tmp ; cat >> tmp/svr.sh ; cd tmp ; chmod +x svr.sh && wget -q %s$file && ./svr.sh on" """ % ss
        #acmd += """echo "AAA" ; cat svr.sh | ssh $cred$node "set -x ;\n cat >> svr.sh ;\n chmod +x svr.sh ;\n echo 'file:'$file ;\n wget -q %s$file && ./svr.sh on" """ % ss
        ret = runpopen( job, acmd, allback=False)
        if ret == 0:
            job.done = True
        ResultQueue.put_nowait(job)
        
    q.put( ('end', job.node, job.comp, job.inst, job.jobid, ret ) )
    retcode = 0
    if type(ret) == type([]):
        retcode = ret[0]
    else:
        retcode = ret
    if retcode > 0:
        sfpath = workdir + 'fail_' + job.jobid
        if not os.path.exists(sfpath):
            sf = open(sfpath , 'w')
            sf.write('fail')
            sf.close()

class casWorkerThread(Thread):
    def __init__(self ):
        Thread.__init__(self) 

    def run(self): 
        global CasJobQueue
        while True:
            job = CasJobQueue.get()
            if job.jobid == 0:
                #print 'end thread'
                break
            do( job )
        
def cascade(jobs, poolsize, rnd = False):
    global ip, port, ResultQueue, CasJobQueue, workdir, dist
    #MaxFailPercent = 10
    targets = []
    targets.extend(jobs)
    totalJobs = len(targets)
    doneCount = 0
    failCount = 0
    dimension = dist
    nodeRetry = 1
    source = 'http://%s:%s/upload/' % (ip, str(port))  
    print "cas source: ", source, " comp: ", jobs[0].comp
    jobid = jobs[0].jobid
    #poolpointer = 0
    pool = []
    for i in range( min( poolsize, len(jobs) ) ):
        cwt = casWorkerThread()
        cwt.start()
        #print 'added thread', i
        pool.append( cwt )
    
    while totalJobs > (doneCount + failCount):
        if os.path.isfile( workdir + 'stop_' + jobid) :
            print 'stopping for: ', jobid
            break
        if len( targets ) > 0:
            if dimension >= len(targets):
                nextTargets = []
                nextTargets.extend(targets)
                targets = []
            else:
                nextTargets = []
                step = int(math.floor(len(targets) / dimension)) -1
                #print 'step:', step
                for i in range(dimension):
                    if rnd:
                        nextTargets.append( targets.pop( randint(0, len(targets) -1 ) ) )
                    else:
                        nextTargets.append( targets.pop( i * step ) )
                #print 'targetlen: ',len(targets)
            
            if len(nextTargets) > 0:
                for t in nextTargets:
                    t.source = source
                    CasJobQueue.put(t)
        else:
            print 'error no targets'
            return
        while totalJobs > (doneCount + failCount):
            if os.path.isfile( workdir + 'stop_' + jobid) :
                print 'stopping for: ', jobid
                break

            resJob = ResultQueue.get()
            if resJob.done:
                doneCount += 1
                source = resJob.node
                print "new source ", source
                if len(targets) > 0 :
                    break  # only proceed seeding if we have a new seed
            else:
                # here it failed so no seed and keep looping
                if resJob.failed >= nodeRetry:
                    failCount += 1
                else:
                    resJob.failed += 1
                    targets.append(resJob)
                    break

    while not CasJobQueue.empty():
        x = CasJobQueue.get()
        print 'remove cjq'
    while not ResultQueue.empty():
        x = ResultQueue.get()
        print 'remove resq'

    for i in range(poolsize):
        j = Job(0, 0, 0, 0, 0)
        CasJobQueue.put(j)
    print 'totalJobs:', totalJobs, 'done:', doneCount, 'fail:', failCount

def teardown( jobid, flat, path, poolsize ):
    global allscripts
    allscripts[ jobid ] = """ ssh $cred@$node "cd tmp; ./svr.sh off ; rm svr.sh ; mv * ../ ; cd .. ; rmdir tmp" """
    jna = []
    for x in flat:
        jna.append( Job( jobid, path, x[1], x[0], '_' ) )
    p = Pool(poolsize)
    p.map(do, jna )
    p.close()
    p.join()

class JobThread(Thread):
    def __init__(self, mode, jobid, script, env, flat, current_client, POOLSIZE=50, GROUPSIZE=0, rnd=False):
        Thread.__init__(self) 
        self.mode = mode
        self.jobid = jobid
        self.script = script
        self.env = env
        self.flat = flat
        self.current_client = current_client
        self.poolsize = POOLSIZE
        self.groupsize = GROUPSIZE
        self.rnd = rnd
        self.scriptdat = ""
        self.counter = 0
        
    def run(self): 
        global jobthreads, counter, allscripts, q, workdir
        print 'JobThread start', self.jobid
        runlogbase = workdir
        path = runlogbase + self.jobid 
        try:
            os.mkdir(path)
        except:
            os.mkdir(workdir)
            os.mkdir(path)
            
        allscripts[ self.jobid ] = getdat( self.script, 'Script' )
        # add jobid to node:app list
        jna = []
        for x in self.flat:
            print x
            try:
                jna.append( Job( self.jobid, path, x[0], x[1], x[2], '_' ) )
            except:
                pass
        
        qt = QThread(self.jobid, self.current_client )
        qt.start()

        sfpath = workdir + 'fail_' + self.jobid
        if self.mode == 'parallel' or self.mode == 'single' or self.mode == 'sequential':
            p = Pool(self.poolsize)
            p.map(do, jna )
            p.close()
            p.join()
        elif self.mode == '2/3 available':
            cnt = len(jna)
            if cnt < 2:
                print 'Error groups: need more than one node!'
                #return 1
            else:
                lgroupsize = max(1,int(math.ceil(cnt / self.groupsize )))
                if cnt > 3 and cnt % 3 > 0:
                    lgroupsize += 1
                print cnt, 'groupsize', lgroupsize, 'steps: ', cnt % self.groupsize
                gcnt = 0
                while gcnt < cnt:
                    jnag = jna[ gcnt : gcnt + lgroupsize ]
                    p = Pool(self.poolsize)
                    p.map(do, jnag )
                    p.close()
                    p.join()
                    if os.path.exists(sfpath):
                        print 'Error group failed on %d : %d' % (gcnt, gcnt + lgroupsize) 
                        break
                    gcnt += lgroupsize
        elif self.mode.find('dist global') > -1:    # run over all
            cascade(jna, self.poolsize, rnd=self.rnd)
            teardown( self.jobid, self.flat, path, self.poolsize )

        elif self.mode.find('dist component') > -1: # split per component
            cmap = {}
            for job in jna:
                svr = job.node
                cmp = job.comp
                if not cmap.has_key(cmp):
                    clist = [ job ]
                    cmap[ cmp ] = clist
                else:
                    clist = cmap[cmp]
                    clist.append( job )
                    cmap[ cmp ] = clist
            for c in cmap.keys():
                #print "dist comp", c, cmap[ c ]
                allscripts[ self.jobid ] = getdat( self.script, 'Script' )
                cascade( cmap[ c ], self.poolsize, rnd=self.rnd )

                mapcopy = []
                for i in self.flat:
                    if i[1] == c:
                        mapcopy.append( i )
                teardown( self.jobid, mapcopy, path, self.poolsize )

        #print 'done', counter
        jobthreads.pop(self.jobid)
        if os.path.exists( workdir + 'stop_' + self.jobid):
            os.popen('rm ' + workdir + 'stop_' + self.jobid )
        if os.path.exists(sfpath):
            os.popen('rm ' + sfpath )
            
        q.put( ('stop', self.jobid ) )
        
        while not q.empty():
            time.sleep(1)

        x = { 'execstate': 'finished','jobid': self.jobid }
        if os.path.isfile(workdir + 'stop_' + self.jobid):
            x = { 'execstate': 'terminated','jobid': self.jobid }
            os.popen('rm ' + workdir + 'stop_' + self.jobid)
        self.current_client.ws.send( json.dumps(x) )
        print 'JobThread end', self.jobid
 

def license():
    print LICENSE

def usage():
    print """
Blitzer version %s 2016 Copyright Intrasts Cologne
    usage: blitzer opts
    opts:
    -h            print this help
    -l            show license
    -i ip4        IP address to listen on ( 127.0.0.1 )
    -p n          port number ( 8888 )
    -d path       data dir ( ./ )
    -w path       working dir ( /tmp/blitzer )
    -t n          threads ( 50 )
    """ % VERSION

def doapp(argv):
    global ip, port, threads, datadir, workdir, envroot, scrroot
    try:                                
        opts, args = getopt.getopt(argv, "hli:p:d:w:t:", [])
    except getopt.GetoptError:          
        usage()                         
        sys.exit(2)                     
    for opt, arg in opts:                
        if opt == "-h":
            usage()
            sys.exit(0)
        elif opt == "-l":
            license()
            sys.exit(0)
        elif opt == '-i':
            ip = arg              
        elif opt == '-p':
            port = int(arg)
        elif opt == '-d':
            datadir = arg            
            envroot = datadir + '/env/' 
            scrroot = datadir + '/scr/' 
        elif opt == '-w':
            workdir = arg              
        elif opt == '-t':
            threads = int(arg)              
    
    print 'working from path: ', workdir
    print 'data from path: ', datadir
    server = WSGIServer( (ip, port), app, handler_class=WebSocketHandler)
    print 'ready to serve on %s:%s threads: %s' % (ip , port, threads ) 
    
    server.serve_forever()

if __name__ == '__main__':
    doapp(sys.argv[1:])    
